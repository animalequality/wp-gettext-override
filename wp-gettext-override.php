<?php

/**
 * Plugin Name: WP Gettext Override
 * Plugin URI: https://gitlab.com/animalequality/wp-gettext-override
 * Description: Overrides Gettext functions and retrieve translations from DB (-> independent of user language)
 * Version: 0.3
 * Author: Daniel Schmid, Animal Equality
 * Author URI: https://gitlab.com/animalequality
 * License: GPL-3.0-or-later
 */

if (!defined('WPGTO_PATH')) {
    define('WPGTO_PATH', plugin_dir_path(__FILE__));
}

require_once WPGTO_PATH . 'includes/AdminPage.php';
require_once WPGTO_PATH . 'includes/TranslationService.php';
require_once WPGTO_PATH . 'includes/GettextFilters.php';
require_once WPGTO_PATH . 'includes/GettextOverridePlugin.php';

new \WPGTO\GettextOverridePlugin();
