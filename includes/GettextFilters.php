<?php

namespace WPGTO;

class GettextFilters
{
    private $translationService;

    public function __construct(TranslationService $translationService)
    {
        $this->translationService = $translationService;
        add_filter('gettext_with_context', [$this, 'gettextWithContext'], 10, 4);
        add_filter('gettext', [$this, 'gettext'], 10, 3);
    }

    public function gettextWithContext($translation, $text, $context, $domain)
    {
        return $domain === 'WPGTO'
            ? $this->translationService->getTranslation($text, $context)
            : $translation;
    }

    public function gettext($translation, $text, $domain)
    {
        return $this->gettextWithContext($translation, $text, 'default', $domain);
    }
}
