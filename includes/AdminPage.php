<?php

namespace WPGTO;

class AdminPage
{
    private $plugin;

    public function __construct($plugin)
    {
        $this->plugin = $plugin;
        add_action('admin_menu', [$this, 'addSettingsPage']);
        add_action('admin_init', [$this, 'addSettings']);
    }

    public function addSettingsPage()
    {
        add_submenu_page(
            'options-general.php',
            'Gettext Override',
            'Gettext Override',
            'administrator',
            'wp-gettext-override',
            [$this, 'renderSettingsPage']
        );
    }

    public function addSettings()
    {
        register_setting($this->plugin->getOptionName(), $this->plugin->getOptionName(), ['sanitize_callback' => [$this, 'sanitizeFieldCallback']]);
        add_settings_section('default', '', null, $this->plugin->getOptionName());

        $translations = get_option($this->plugin->getOptionName(), []);

        foreach ($translations as $text_key => $text_value) {
            foreach ($text_value['contexts'] as $context_key => $context_value) {
                $id = sprintf('%s_%s', $text_key, $context_key);

                add_settings_field(
                    $id,
                    $text_value['text'],
                    [$this, 'renderFieldCallback'],
                    $this->plugin->getOptionName(),
                    'default',
                    [
                        'text_key' => $text_key,
                        'context_key' => $context_key,
                        'translation' => $context_value['translation'],
                        'label_for' => $id,
                        'context' => $context_value['context'],
                    ]
                );
            }
        }
    }

    public function renderSettingsPage()
    {
        if (!current_user_can('administrator')) {
            return;
        }

        ?>
            <div class="wrap">
                <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
                <form action="options.php" method="post">
        <?php
                settings_fields($this->plugin->getOptionName());
                do_settings_sections($this->plugin->getOptionName());
                submit_button();
        ?>
                </form>
            </div>
        <?php
    }

    public function sanitizeFieldCallback(?array $translations): array
    {
        $currentTranslations = get_option($this->plugin->getOptionName());
        if (is_array($currentTranslations) && is_array($translations)) {
            return array_replace_recursive($currentTranslations, $translations);
        }
        return is_array($currentTranslations) ? $currentTranslations : [];
    }

    public function renderFieldCallback($args)
    {
        $name = sprintf('%s[%s][contexts][%s][translation]', $this->plugin->getOptionName(), $args['text_key'], $args['context_key']);
        ?>
            <input type="text" id="<?php echo $args['label_for']; ?>" name="<?php echo $name; ?>" value="<?php echo $args['translation']; ?>">
            <p class="description">
                <strong>Context:</strong> <?php echo $args['context']; ?>
            </p>
        <?php
    }
}
