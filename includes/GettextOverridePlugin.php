<?php

namespace WPGTO;

class GettextOverridePlugin
{
    private const OPTION_NAME = 'gettext_override';

    public function __construct()
    {
        add_action('init', [$this, 'initPlugin'], 5);
    }

    public function initPlugin()
    {
        $translationService = new TranslationService($this);
        new GettextFilters($translationService);

        if (is_admin()) {
            new AdminPage($this);
        }
    }

    public function getOptionName(): string
    {
        return self::OPTION_NAME;
    }
}
