<?php

namespace WPGTO;

class TranslationService
{
    private $plugin;

    public function __construct($plugin)
    {
        $this->plugin = $plugin;
    }

    private function extractTranslation(array $translations, string $text_key, string $context_key): ?string
    {
        $translation = $translations[$text_key] ?? [];

        if (
            !array_key_exists($text_key, $translations) ||
            !array_key_exists('contexts', $translation) ||
            !array_key_exists($context_key, $translation['contexts'])
        ) {
            return null;
        }

        return $translation['contexts'][$context_key]['translation'];
    }

    private function addTranslation(array $translations, string $text_key, string $context_key, string $text, string $context)
    {
        if (!isset($translations[$text_key]['text'])) {
            $translations[$text_key]['text'] = $text;
        }

        $translations[$text_key]['contexts'][$context_key] = [
            'context' => $context,
            'translation' => '',
        ];

        update_option($this->plugin->getOptionName(), $translations);
    }

    public function getTranslation(string $text, string $context): string
    {
        $translations = get_option($this->plugin->getOptionName(), []);
        $text_key = sanitize_key($text);
        $context_key = $context ? sanitize_key($context) : 'default';
        $translation = $this->extractTranslation($translations, $text_key, $context_key);

        if ($translation === null) {
            $this->addTranslation($translations, $text_key, $context_key, $text, $context);
        }

        return $translation ? $translation : $text;
    }
}
